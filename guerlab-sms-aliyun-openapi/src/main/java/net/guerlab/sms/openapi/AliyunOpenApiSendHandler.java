/*
 * Copyright 2018-2023 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.sms.openapi;

import java.util.Collection;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.dysmsapi20170525.models.SendSmsResponseBody;
import com.aliyun.teaopenapi.models.Config;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import org.springframework.context.ApplicationEventPublisher;

import net.guerlab.sms.core.domain.NoticeData;
import net.guerlab.sms.core.exception.SendFailedException;
import net.guerlab.sms.core.utils.StringUtils;
import net.guerlab.sms.server.handler.AbstractSendHandler;

/**
 * 阿里云短信发送处理.
 *
 * @author guer
 */
@Slf4j
public class AliyunOpenApiSendHandler extends AbstractSendHandler<AliyunOpenApiProperties> {

	private static final String OK = "OK";

	private static final String DOMAIN = "dysmsapi.aliyuncs.com";

	private final ObjectMapper objectMapper;

	private final Client client;

	/**
	 * 构造阿里云短信发送处理.
	 *
	 * @param properties     阿里云短信配置
	 * @param eventPublisher spring应用事件发布器
	 * @param objectMapper   objectMapper
	 */
	public AliyunOpenApiSendHandler(AliyunOpenApiProperties properties, ApplicationEventPublisher eventPublisher,
			ObjectMapper objectMapper) {
		super(properties, eventPublisher);
		this.objectMapper = objectMapper;

		String accessKeyId = properties.getAccessKeyId();
		String accessKeySecret = properties.getAccessKeySecret();

		Config config = new Config().setAccessKeyId(accessKeyId).setAccessKeySecret(accessKeySecret)
				.setEndpoint(DOMAIN);
		try {
			client = new Client(config);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean send(NoticeData noticeData, Collection<String> phones) {
		String paramString;
		try {
			paramString = objectMapper.writeValueAsString(noticeData.getParams());
		}
		catch (Exception e) {
			log.debug(e.getMessage(), e);
			publishSendFailEvent(noticeData, phones, e);
			return false;
		}

		SendSmsRequest request = new SendSmsRequest();
		request.setPhoneNumbers(StringUtils.join(phones, ","));
		request.setSignName(properties.getSignName());
		request.setTemplateCode(properties.getTemplates(noticeData.getType()));
		request.setTemplateParam(paramString);

		try {
			SendSmsResponse sendSmsResponse = client.sendSms(request);
			SendSmsResponseBody responseBody = sendSmsResponse.getBody();

			if (OK.equals(responseBody.getCode())) {
				publishSendSuccessEvent(noticeData, phones);
				return true;
			}

			log.debug("send fail[code={}, message={}]", responseBody.getCode(), responseBody.getMessage());

			publishSendFailEvent(noticeData, phones, new SendFailedException(responseBody.getMessage()));
		}
		catch (Exception e) {
			log.debug(e.getMessage(), e);
			publishSendFailEvent(noticeData, phones, e);
		}
		return false;
	}

	@Override
	public String getChannelName() {
		return "aliyun-openapi";
	}
}
